#include "../include/MPIExe.h"
#include "../include/OMPExe.h"
#include <mpi.h>
#include <cstdlib>
#include <omp.h>
#include <chrono>
using namespace std::chrono;
using namespace std;

MPIExe::MPIExe(OMPExe ompexe)
{
    MPI_Comm_rank (MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    srand(time(0));
    N = 21000000;
    if (rank == 0) {
        cout << "MPI CPUs number: " << size << endl;

        data_vector = new int[2 * N];
        for(unsigned i = 0; i < 2 * N; i++) {
            data_vector[i] = rand() % 100;
        }
        cout << "ok"<< endl;
    } else {
        data_vector = new int[2 * N / (size - 1)];
    }
    result = 0;
    time_spent = 0;
}

MPIExe::~MPIExe()
{

}


void MPIExe::process()
{
    MPI_Status status;
    result = 0;
    time_spent = 0;
    int piece_size = 2 * N / (size - 1);
    if (rank == 0) {
        double start = clock();
        cout << "Avaible " << size <<" CPUs." << endl;

        for(int i = 1; i < size; i++) {
            MPI_Send(&data_vector[(i - 1) * piece_size], piece_size, MPI_INT, i, 1, MPI_COMM_WORLD);
        }

        for(int i = 1; i < size; i++) {
            long tmp_result = 0;
            MPI_Recv(&tmp_result, sizeof(long), MPI_INT, i, 2, MPI_COMM_WORLD, &status);
            result += tmp_result;
        }

        double end = clock();
        time_spent = (end-start)/CLOCKS_PER_SEC;
    } else {
        MPI_Recv(data_vector, piece_size, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);

        for(int i = 0; i < piece_size; i+=2) {
            result += data_vector[i] * data_vector[i + 1];
        }

        MPI_Send(&result, sizeof(long), MPI_INT, 0, 2, MPI_COMM_WORLD);
    }
}

double MPIExe::get_time()
{
    return time_spent;
}

long MPIExe::get_result()
{
    return result;
}
