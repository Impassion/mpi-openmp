#include "../include/OMPExe.h"
#include <cstdlib>
#include <omp.h>
#include <chrono>
using namespace std::chrono;

OMPExe::OMPExe()
{
    N = 21000000;
    srand(time(0));
    vector1.reserve(N);
    vector2.reserve(N);
    for(unsigned i = 0; i < N; i++) {
        vector1[i] = rand() % 100;
        vector2[i] = rand() % 100;
    }
    result = 0;
    time_spent = 0;
}

OMPExe::~OMPExe()
{
}

void OMPExe::process()
{
    result = 0;
    time_spent = 0;
    auto start = steady_clock::now();
    long tmp_result = 0;
#pragma omp parallel for reduction(+:tmp_result)
    for(unsigned i = 0; i < N; i++) {
        tmp_result += vector1[i]*vector2[i];
    }
    result = tmp_result;
    auto end = steady_clock::now();
    time_spent = duration_cast<milliseconds>(end-start).count();
}

int OMPExe::get_time()
{
    return time_spent;
}

long OMPExe::get_result()
{
    return result;
}
