#include <iostream>
#include "include/OMPExe.h"
#include "include/MPIExe.h"
#include <mpi.h>

using namespace std;

int main(int argc, char *argv[])
{
    MPI_Init(&argc,&argv);
    int rank = 0;
    MPI_Comm_rank (MPI_COMM_WORLD, &rank);
    OMPExe ompexe;
    if (rank == 0) {
        ompexe.process();
        cout << "OpenMP result: " << ompexe.get_result() << ". Time: " << ompexe.get_time() << endl;
    }
    MPIExe mpiexe(ompexe);
    mpiexe.process();
    if (rank == 0) {
        cout << "OpenMP result: " << mpiexe.get_result() << ". Time: " << mpiexe.get_time() << endl;
    }
    MPI_Finalize();
    return 0;
}
