#ifndef OMPEXE_H
#define OMPEXE_H
#include <iostream>
#include <vector>
using namespace std;


class OMPExe
{
    public:
        OMPExe();
        ~OMPExe();
        void process();
        int get_time();
        long get_result();
        vector<int> vector1, vector2;
    private:
        int time_spent;
        long result;
        unsigned long N;
};

#endif // OMPEXE_H
