#ifndef MPIEXE_H
#define MPIEXE_H
#include "OMPExe.h"
#include <iostream>
#include <vector>
using namespace std;

class MPIExe
{
    public:
        MPIExe(OMPExe ompexe);
        ~MPIExe();
    void process();
        double get_time();
        long get_result();
    private:
        int* data_vector;
        double time_spent;
        long result;
        unsigned long N;
        int rank, size;
};

#endif // MPIEXE_H
